# Dollar Fix

For using _inside_ **WSL 2** (Ubuntu) and **Docker Desktop + Lando** installed on Windows 10

## Requirements

Needs Docker Desktop to be integrated fully into WSL.

## Install

Create the `plugins/` folder inside `.lando/`:

```ps
mkdir .\.lando\plugins
```

Clone or copy the plugin into the folder:

```ps
cp .\dollar-fix\ .\.lando\plugins\
```

## Credits

The plugin was originally conceived by [@JensYvanDeCraecker](https://github.com/JensYvanDeCraecker).
