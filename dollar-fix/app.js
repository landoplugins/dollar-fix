'use strict';

const path = require('path');

module.exports = (app, lando) => {
    app.configFiles = app.configFiles.map(escape);
    app.root = escape(app.root);
    
    return {
      env: {
        LANDO_APP_ROOT: app.root,
        LANDO_APP_ROOT_BIND: app.root,
      },
      labels: {
        "io.lando.src": app.configFiles.join(","),
      },
    };
};
  
function escape(path) {
    return path.replace("$", "$$$"); 
}
